package vn.geekup.android.skeleton.network;

import android.app.IntentService;
import android.content.Intent;

/**
 * Created by H.Anh on 6/10/2015.
 */
public class NetworkService extends IntentService {

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public NetworkService() {
        super("Network Service");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }
}
