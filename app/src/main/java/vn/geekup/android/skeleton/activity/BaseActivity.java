package vn.geekup.android.skeleton.activity;

import android.app.Activity;
import android.os.Bundle;

import vn.geekup.android.skeleton.util.InjectView;

/**
 * Created by H.Anh on 06/05/2015.
 */
public abstract class BaseActivity extends Activity {

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

        InjectView.inject(this);
    }
}
