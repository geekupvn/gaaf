package vn.geekup.android.skeleton.util;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;

import java.lang.reflect.Field;

import vn.geekup.android.skeleton.annotation.ViewId;

/**
 * Created by H.Anh on 06/05/2015.
 */
public class InjectView {

    public static <T extends View> T findById(View view, int viewId) {
        return (T) view.findViewById(viewId);
    }

    public static void inject(Object object) {
        inject(object, object);
    }

    public static void inject(Object target, Object source) {
        Field[] fields = target.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(ViewId.class)) {
                ViewId annotation = field.getAnnotation(ViewId.class);
                int viewId = annotation.value();

                View view = null;
                if (source instanceof Activity) {
                    view = ((Activity) source).findViewById(viewId);
                } else if (source instanceof View) {
                    view = ((View) source).findViewById(viewId);
                } else if (source instanceof Dialog) {
                    view = ((Dialog) source).findViewById(viewId);
                }

                try {
                    field.setAccessible(true);
                    field.set(target, view);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
