package vn.geekup.android.skeleton.model;

import java.util.ArrayList;
import java.util.List;

import co.uk.rushorm.core.annotations.RushList;
import co.uk.rushorm.core.annotations.RushTableAnnotation;

/**
 * Created by H.Anh on 6/10/2015.
 */
@RushTableAnnotation
public class Club extends RushModel {

    private String name;

    @RushList(classType = Student.class)
    private List<Student> students;

    public Club() {

    }

    public Club(String name) {
        this.name = name;
        this.students = new ArrayList<>();
    }

    public void addStudent(Student student) {
        this.students.add(student);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }
}
