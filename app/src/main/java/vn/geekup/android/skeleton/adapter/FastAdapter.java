package vn.geekup.android.skeleton.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

import vn.geekup.android.skeleton.holder.BaseHolder;
import vn.geekup.android.skeleton.model.BaseModel;

/**
 * Created by H.Anh on 06/05/2015.
 */
public class FastAdapter<T extends BaseModel, M extends BaseHolder> extends BaseAdapter {

    private List<T> data;
    private List<Class<M>> holders;
    private List<Integer> resources;
    private int viewTypeCount = 1;

    public FastAdapter(@NonNull List<T> data, @NonNull Class<M> holder, @NonNull @LayoutRes int resource) {
        this.data = data;
        this.holders = Arrays.asList(holder);
        this.resources = Arrays.asList(resource);
    }

    public FastAdapter(@NonNull List<T> data, @NonNull List<Class<M>> holders, @NonNull @LayoutRes List<Integer> resources) {
        this.data = data;
        this.holders = holders;
        this.resources = resources;
    }

    @Override
    public int getViewTypeCount() {
        return viewTypeCount;
    }

    public void setViewTypeCount(int viewTypeCount) {
        this.viewTypeCount = viewTypeCount;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            T model = data.get(position);

            Context context = parent.getContext();
            if (convertView == null) {
                Integer type = model.getViewType();
                int countTypeView = getViewTypeCount();
                if (type >= countTypeView) {
                    type = countTypeView - 1;
                }

                Integer resource = resources.get(type);
                convertView = View.inflate(context, resource, null);

                Class<M> holder = holders.get(type);
                Constructor<M> constructor = holder.getConstructor(View.class);
                BaseHolder baseHolder = constructor.newInstance(convertView);

                baseHolder.mAdapter = this;
                baseHolder.mContext = context;
                baseHolder.mData = data;

                convertView.setTag(baseHolder);
            }

            BaseHolder baseHolder = (BaseHolder) convertView.getTag();
            baseHolder.mPosition = position;
            baseHolder.mView = convertView;

            baseHolder.bind(model);

            return convertView;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }
}
