package vn.geekup.android.skeleton.model;

import java.util.List;

import co.uk.rushorm.core.annotations.RushList;

/**
 * Created by H.Anh on 6/10/2015.
 */
public class Items extends RushModel {

    @RushList(classType = Item.class)
    private List<Item> items;

    public Items(List<Item> items) {
        this.items = items;
    }
}
