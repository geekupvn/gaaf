package vn.geekup.android.skeleton.model;

import co.uk.rushorm.core.RushObject;
import co.uk.rushorm.core.RushSearch;
import co.uk.rushorm.core.annotations.RushTableAnnotation;

/**
 * Created by H.Anh on 6/5/2015.
 */
@RushTableAnnotation
public class RushModel extends RushObject {

    public RushModel() {

    }

    public static RushSearch search() {
        return new RushSearch();
    }
}
