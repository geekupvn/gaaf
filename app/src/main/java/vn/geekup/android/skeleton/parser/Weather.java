package vn.geekup.android.skeleton.parser;

import com.google.gson.annotations.SerializedName;

import vn.geekup.android.skeleton.R;
import vn.geekup.android.skeleton.annotation.ViewId;
import vn.geekup.android.skeleton.model.BaseModel;

/**
 * Created by H.Anh on 6/10/2015.
 */
public class Weather {

    public int id;

    @SerializedName("main")
    public String status;

    public String description;
    public String icon;

    @Override
    public String toString() {
        return status;
    }
}
