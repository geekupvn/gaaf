package vn.geekup.android.skeleton.parser;

/**
 * Created by H.Anh on 6/10/2015.
 */
public class City {

    public static class Coord {
        public float lon;
        public float lat;
    }

    public String id;
    public String name;

    public Coord coord;
    public String country;
    public int population;
}
