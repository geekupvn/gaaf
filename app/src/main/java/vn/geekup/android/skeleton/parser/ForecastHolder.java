package vn.geekup.android.skeleton.parser;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import vn.geekup.android.skeleton.R;
import vn.geekup.android.skeleton.annotation.OnClick;
import vn.geekup.android.skeleton.annotation.OnItemClick;
import vn.geekup.android.skeleton.annotation.OnItemLongClick;
import vn.geekup.android.skeleton.annotation.OnLongClick;
import vn.geekup.android.skeleton.annotation.ViewId;
import vn.geekup.android.skeleton.holder.BaseHolder;

/**
 * Created by H.Anh on 6/10/2015.
 */
public class ForecastHolder extends BaseHolder<Forecast> {

    @ViewId(R.id.txtId)
    public TextView txtId;

    @ViewId(R.id.txtName)
    public TextView txtName;

    public ForecastHolder(View view) {
        super(view);
    }

    @OnItemLongClick
    void onItemLongClick() {
        Toast.makeText(mContext, "Long Click", Toast.LENGTH_SHORT).show();
        mData.remove(mPosition);
        mAdapter.notifyDataSetChanged();
    }

    @OnItemClick
    void onItemClick() {
        Toast.makeText(mContext, "Click", Toast.LENGTH_SHORT).show();
        mData.remove(mPosition);
        mAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.txtName)
    void onNameClick(int viewId) {
        switch (viewId) {
            case R.id.txtId:
                Toast.makeText(mContext, txtId.getText(), Toast.LENGTH_SHORT).show();
                break;
            case R.id.txtName:
                Toast.makeText(mContext, txtName.getText(), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @OnLongClick(R.id.txtName)
    void rename(int viewId) {
        txtName.setText("OK");
    }

    @Override
    public void bind(Forecast model) {
        super.bind(model);

    }
}
