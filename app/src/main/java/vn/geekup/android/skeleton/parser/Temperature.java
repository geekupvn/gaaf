package vn.geekup.android.skeleton.parser;

import com.google.gson.annotations.SerializedName;

/**
 * Created by H.Anh on 6/10/2015.
 */
public class Temperature {

    public float day;
    public float min;
    public float max;
    public float night;

    @SerializedName("evening")
    public float eve;

    @SerializedName("morn")
    public float morning;
}
