package vn.geekup.android.skeleton.element;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import vn.geekup.android.skeleton.R;

public class CustomButton extends Button {

    public static final int HEAVY = 0;
    public static final int LIGHT = 1;
    public static final int REGULAR = 2;
    public static final int THIN = 3;

    public CustomButton(Context context) {
        super(context);

        if (isInEditMode()) {
            return;
        }

        setFont();
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (isInEditMode()) {
            return;
        }

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomTextView,
                0, 0);

        int customType = -1;
        try {
            customType = a.getInteger(R.styleable.CustomTextView_custom_style, -1);
        } finally {
            a.recycle();
        }

        setFont(customType);
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        if (isInEditMode()) {
            return;
        }

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomTextView,
                0, 0);

        int customType = -1;
        try {
            customType = a.getInteger(R.styleable.CustomTextView_custom_style, -1);
        } finally {
            a.recycle();
        }
        setFont(customType);
    }

    private void setFont(int type) {
        String fontLocation;
        switch (type) {
            case HEAVY:
                fontLocation = "fonts/Lato-Heavy.ttf";
                break;
            case REGULAR:
                fontLocation = "fonts/Lato-Regular.ttf";
                break;
            case THIN:
                fontLocation = "fonts/Lato-Thin.ttf";
                break;
            default:
                fontLocation = "fonts/Lato-Light.ttf";
                break;
        }

        Typeface font = Typeface.createFromAsset(getContext().getAssets(),
                fontLocation);

        setTypeface(font);
    }

    private void setFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Lato-Light.ttf");
        setTypeface(font);
    }

}
