package vn.geekup.android.skeleton.model;

import co.uk.rushorm.core.annotations.RushTableAnnotation;
import vn.geekup.android.skeleton.R;
import vn.geekup.android.skeleton.annotation.ViewId;

/**
 * Created by H.Anh on 06/05/2015.
 */
@RushTableAnnotation
public class Student extends BaseModel {

    @ViewId(R.id.txtId)
    public String id;

    @ViewId(R.id.txtName)
    public String name;

    public Student() {

    }

    public Student(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Student(String id, String name, int type) {
        this.id = id;
        this.name = name;
        setViewType(type);
    }
}
