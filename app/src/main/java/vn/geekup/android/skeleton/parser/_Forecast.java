package vn.geekup.android.skeleton.parser;

import java.util.List;

/**
 * Created by H.Anh on 6/10/2015.
 */
public class _Forecast {

    public String cod;
    public float message;

    public City city;

    public int cnt;
    public List<Forecast> list;
}
