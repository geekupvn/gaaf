package vn.geekup.android.skeleton.model;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import co.uk.rushorm.core.annotations.RushIgnore;
import vn.geekup.android.skeleton.annotation.ViewId;

/**
 * Created by H.Anh on 06/05/2015.
 */
public class BaseModel extends RushModel {

    @RushIgnore
    private static transient final Map<Class, Map<Integer, Object>> injectCached = new HashMap<>();

    @RushIgnore
    protected transient Map<Integer, Object> mFields;

    @RushIgnore
    protected transient int viewType = 0;

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public BaseModel() {
        init();
    }

    public void init() {
        Class<? extends BaseModel> clazz = this.getClass();

        mFields = injectCached.get(clazz);

        if (mFields == null) {
            mFields = new HashMap<>();

            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(ViewId.class)) {
                    ViewId annotation = field.getAnnotation(ViewId.class);
                    int viewId = annotation.value();
                    mFields.put(viewId, field);
                }
            }

            Method[] methods = clazz.getDeclaredMethods();
            for (Method method : methods) {
                if (method.isAnnotationPresent(ViewId.class)) {
                    ViewId annotation = method.getAnnotation(ViewId.class);
                    int viewId = annotation.value();
                    mFields.put(viewId, method);
                }
            }

            injectCached.put(clazz, mFields);
        }
    }

    public String getValue(int viewId) {
        try {
            Object obj = mFields.get(viewId);
            if (obj == null) {
                return null;
            }

            if (obj instanceof Field) {
                Field field = (Field) obj;
                field.setAccessible(true);
                Object value = field.get(this);
                return value == null ? "" : value.toString();
            }

            if (obj instanceof Method) {
                Method method = (Method) obj;
                method.setAccessible(true);
                Object callback = method.invoke(this);
                return callback == null ? "" : callback.toString();
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }
}
