package vn.geekup.android.skeleton.activity;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.List;

import vn.geekup.android.skeleton.R;
import vn.geekup.android.skeleton.adapter.FastAdapter;
import vn.geekup.android.skeleton.annotation.ViewId;
import vn.geekup.android.skeleton.holder.BaseHolder;
import vn.geekup.android.skeleton.holder.ViewHolder;
import vn.geekup.android.skeleton.model.Club;
import vn.geekup.android.skeleton.model.Student;
import vn.geekup.android.skeleton.parser.Forecast;
import vn.geekup.android.skeleton.parser.ForecastHolder;
import vn.geekup.android.skeleton.parser.Weather;
import vn.geekup.android.skeleton.parser._Forecast;

/**
 * Created by H.Anh on 06/05/2015.
 */

public class MainActivity extends BaseActivity {

    @ViewId(R.id.text)
    private EditText text;

    @ViewId(R.id.button)
    private Button button;

    @ViewId(R.id.listView)
    private ListView listView;

    private FastAdapter adapter;
    private List<Student> data;

    private Club club;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_activity);

        Ion.with(this).load("http://api.openweathermap.org/data/2.5/forecast/daily?q=Hanoi,vn&cnt=5")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        Gson gson = new GsonBuilder().setPrettyPrinting().create();
                        final _Forecast forecast = gson.fromJson(result, _Forecast.class);

                        String s = gson.toJson(forecast);
                        Log.e("Json", s);

                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                FastAdapter adapter = new FastAdapter(forecast.list, ForecastHolder.class, R.layout.list_item);
                                listView.setAdapter(adapter);
                            }
                        });
                    }
                });

//        List<Club> clubs = Club.search().find(Club.class);
//        if (clubs.isEmpty()) {
//            Log.e("Club", "Empty");
//            club = new Club("Android");
//            club.save();
//        } else {
//            Log.e("Club", "Has Data");
//            club = clubs.get(0);
//        }
//
//        data = club.getStudents();
//
//        adapter = new FastAdapter(data, ViewHolder.class, R.layout.list_item);
//
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Student student = new Student(club.getId(), text.getText().toString());
//                club.addStudent(student);
//                club.save();
//                data.add(student);
//                adapter.notifyDataSetChanged();
//            }
//        });
//
////        List<Class> holders = new ArrayList<>();
////        holders.add(ViewHolder.class);
////        holders.add(ViewHolder2.class);
////        List<Integer> resources = Arrays.asList(R.layout.list_item, R.layout.list_item2);
////        adapter = new FastAdapter(data, holders, resources);
//
//        listView.setAdapter(adapter);
    }
}
