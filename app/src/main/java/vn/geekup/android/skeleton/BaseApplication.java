package vn.geekup.android.skeleton;

import android.app.Application;

import co.uk.rushorm.android.RushAndroid;

/**
 * Created by H.Anh on 6/5/2015.
 */
public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        RushAndroid.initialize(this);
    }
}
