package vn.geekup.android.skeleton.holder;

import android.content.Context;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import vn.geekup.android.skeleton.annotation.OnClick;
import vn.geekup.android.skeleton.annotation.OnItemClick;
import vn.geekup.android.skeleton.annotation.OnItemLongClick;
import vn.geekup.android.skeleton.annotation.OnLongClick;
import vn.geekup.android.skeleton.annotation.ViewId;
import vn.geekup.android.skeleton.model.BaseModel;

/**
 * Created by H.Anh on 06/05/2015.
 */
public class BaseHolder<T extends BaseModel> {

    private static final Map<Class, Map<Integer, List<Meta>>> injectCached = new HashMap<>();

    public View mView;
    public int mPosition;
    public Context mContext;
    public BaseAdapter mAdapter;
    public List<T> mData;

    protected static class Meta {
        Object object;
        Annotation annotation;

        public Meta(Object object, Annotation annotation) {
            this.object = object;
            this.annotation = annotation;
        }

    }

    protected Map<Integer, List<Meta>> mFields;

    public BaseHolder(View view) {
        init();
        inject(view);
    }

    private void inject(final View view) {
        final Object _context = this;

        for (final Integer viewId : mFields.keySet()) {
            final List<Meta> lstMeta = mFields.get(viewId);

            final boolean isMView = viewId == ITEM_CLICK || viewId == ITEM_LONG_CLICK;
            View v = isMView ? view : view.findViewById(viewId);

            if (!isMView) {
                for (Meta meta : lstMeta) {
                    if (meta.annotation instanceof ViewId) {
                        Field field = (Field) meta.object;
                        try {
                            field.setAccessible(true);
                            field.set(_context, v);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                }
            }

            final List<Meta> forClick = new ArrayList<>();
            final List<Meta> forLongClick = new ArrayList<>();

            for (Meta meta : lstMeta) {
                if (meta.annotation instanceof OnClick || meta.annotation instanceof OnItemClick) {
                    forClick.add(meta);
                } else if (meta.annotation instanceof OnLongClick || meta.annotation instanceof OnItemLongClick) {
                    forLongClick.add(meta);
                }
            }

            if (!forClick.isEmpty()) {
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (Meta meta : forClick) {
                            Method method = (Method) meta.object;
                            method.setAccessible(true);
                            try {
                                if (meta.annotation instanceof OnItemClick) {
                                    method.invoke(_context);
                                } else {
                                    method.invoke(_context, viewId);
                                }
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            } catch (InvocationTargetException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }

            if (!forLongClick.isEmpty()) {
                v.setLongClickable(true);
                v.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        for (Meta meta : forLongClick) {
                            final Method method = (Method) meta.object;
                            method.setAccessible(true);

                            try {
                                if (meta.annotation instanceof OnItemLongClick) {
                                    method.invoke(_context);
                                } else {
                                    method.invoke(_context, viewId);
                                }

                                return true;
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            } catch (InvocationTargetException e) {
                                e.printStackTrace();
                            }


                            break;
                        }

                        return false;
                    }
                });
            }
        }
    }

    private static final int ITEM_CLICK = -1;
    private static final int ITEM_LONG_CLICK = -2;

    private static final void addMethod(Map<Integer, List<Meta>> mFields, Integer viewId, Method method, Annotation annotation) {
        List<Meta> lst = mFields.get(viewId);

        Meta meta = new Meta(method, annotation);
        if (lst == null) {
            lst = new ArrayList<>();
            mFields.put(viewId, lst);
        }

        lst.add(meta);
    }

    public void init() {
        Class<? extends BaseHolder> clazz = this.getClass();

        mFields = injectCached.get(clazz);

        if (mFields == null) {
            mFields = new HashMap<>();

            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(ViewId.class)) {
                    ViewId annotation = field.getAnnotation(ViewId.class);
                    int viewId = annotation.value();
                    Meta meta = new Meta(field, annotation);
                    List<Meta> lst = new ArrayList<>();
                    lst.add(meta);
                    mFields.put(viewId, lst);
                }
            }

            Method[] methods = clazz.getDeclaredMethods();
            for (Method method : methods) {
                if (method.isAnnotationPresent(OnClick.class)) {
                    OnClick annotation = method.getAnnotation(OnClick.class);
                    int[] viewIds = annotation.value();
                    for (int viewId : viewIds) {
                        addMethod(mFields, viewId, method, annotation);
                    }
                } else if (method.isAnnotationPresent(OnLongClick.class)) {
                    OnLongClick annotation = method.getAnnotation(OnLongClick.class);
                    int[] viewIds = annotation.value();
                    for (int viewId : viewIds) {
                        addMethod(mFields, viewId, method, annotation);
                    }
                } else if (method.isAnnotationPresent(OnItemClick.class)) {
                    OnItemClick annotation = method.getAnnotation(OnItemClick.class);
                    addMethod(mFields, -1, method, annotation);
                } else if (method.isAnnotationPresent(OnItemLongClick.class)) {
                    OnItemLongClick annotation = method.getAnnotation(OnItemLongClick.class);
                    addMethod(mFields, -2, method, annotation);
                }
            }

            injectCached.put(clazz, mFields);
        }
    }

    public void bind(T model) {
        for (Integer viewId : mFields.keySet()) {
            List<Meta> lstMeta = mFields.get(viewId);
            for (Meta meta : lstMeta) {
                if (meta.annotation instanceof ViewId) {
                    try {
                        Field field = (Field) meta.object;
                        Object object = field.get(this);
                        if (object instanceof TextView) {
                            String text = model.getValue(viewId);
                            String format = ((ViewId) meta.annotation).format();
                            if (!format.isEmpty()) {
                                text = String.format(format, text);
                            }
                            ((TextView) object).setText(text);
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
