package vn.geekup.android.skeleton.parser;

import java.util.List;

import vn.geekup.android.skeleton.R;
import vn.geekup.android.skeleton.annotation.ViewId;
import vn.geekup.android.skeleton.model.BaseModel;

/**
 * Created by H.Anh on 6/10/2015.
 */
public class Forecast extends BaseModel {

    @ViewId(R.id.txtId)
    public int dt;

    public Temperature temp;

    public int pressure;
    public int humidity;

    @ViewId(R.id.txtName)
    private String getWeather() {
        return weather.get(0).status;
    }

    public List<Weather> weather;

    public float speed;
    public int deg;
    public int clouds;
    public float rain;
}
