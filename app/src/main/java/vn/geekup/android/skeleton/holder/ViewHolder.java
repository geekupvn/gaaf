package vn.geekup.android.skeleton.holder;

import android.view.View;
import android.widget.TextView;

import vn.geekup.android.skeleton.R;
import vn.geekup.android.skeleton.annotation.OnClick;
import vn.geekup.android.skeleton.annotation.ViewId;
import vn.geekup.android.skeleton.model.Student;

/**
 * Created by H.Anh on 06/05/2015.
 */
public class ViewHolder extends BaseHolder<Student> {

    @ViewId(value = R.id.txtId, format = " + %s")
    public TextView txtId;

    @ViewId(R.id.txtName)
    public TextView txtName;

    public ViewHolder(View view) {
        super(view);
    }

    @Override
    public void bind(Student model) {
        super.bind(model);


    }
}
